//
//  Case.h
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Place.h"

@interface Case : NSObject

@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) Place *place;
@property (nonatomic, strong) NSString *stringDescription;
@property (nonatomic, strong) NSMutableArray *friends;
@property (nonatomic, strong) NSNumber *rate;
@property (nonatomic, strong) NSNumber *date;
@property (nonatomic, strong) NSMutableArray *comments;

@end
