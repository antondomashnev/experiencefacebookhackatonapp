//
//  SearchList.m
//  InFlow
//
//  Created by Ace Vice on 6/29/11.
//  Copyright 2011 Empatika. All rights reserved.
//

#import "SearchList.h"

@implementation SearchList


-(NSMutableArray*) searchList: (NSMutableArray*) list withText: (NSString*) text{
    if(text!=nil && ![text isEqualToString:@""]){
        
        if([list count]==0)
            return [NSMutableArray array];
        
        if(text!=previousSearchRequest){
            previousSearchRequest = text;
        }
        
        NSMutableArray *searchResults =[[NSMutableArray alloc]init ];    
        
        for(int j=0;j<[list count];++j){
            
            id item = [list objectAtIndex:j];
            NSString *toSearch=nil;  
            NSString *className = NSStringFromClass([item class]);
            
            if([className isEqualToString:@"__NSArrayM"] || [className isEqualToString:@"__NSArrayI"]){
                [searchResults addObject: [self searchList:[list objectAtIndex:j] withText:text]];
            }
            else{
                //toSearch = ((Base*)item).stringDescription;
                NSRange found = [toSearch rangeOfString:text options:NSCaseInsensitiveSearch];
                if(found.location!=NSNotFound)
                    if(found.location==0 || [toSearch characterAtIndex:(found.location-1)]==' ') //we are intereted in matches only in the beginning of the string or beginning of the word
                        [searchResults addObject:item];
            }
        }
        return 
            searchResults;
    }
    else {
        return list;
    }
}
@end
