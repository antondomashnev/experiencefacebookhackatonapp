//
//  SearchList.h
//  InFlow
//
//  Created by Ace Vice on 6/29/11.
//  Copyright 2011 Empatika. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface SearchList : NSObject {
    NSString* previousSearchRequest;
    //NSArray* originalList;
}
-(NSMutableArray*) searchList: (NSMutableArray*) list withText: (NSString*) text;
@end
