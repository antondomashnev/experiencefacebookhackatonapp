//
//  FakeExperienceViewController.h
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FakeExperienceViewController : UIViewController

- (IBAction)doneButtonClicked:(id)sender;

@end
