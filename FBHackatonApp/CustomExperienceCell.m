//
//  CustomExperienceCell.m
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "CustomExperienceCell.h"
#import "StarsRateView.h"
#import "Constants.h"
#import "UIImageView+WebCache.h"
#import "UIColor+AppColor.h"
#import "UIFont+AppFont.h"

#define RATE_BACK_IMG_VIEW_FRAME CGRectMake(100, 84, 100, 33)
#define NAME_BACK_IMG_VIEW_FRAME CGRectMake(0, 84, 100, 33)

#define PERSON_ICON_FRAME CGRectMake(270, 91, 18, 18)
#define RATE_LABEL_FRAME CGRectMake(290, 91, 50, 18)

#define STARS_VIEW_FRAME CGRectMake(100, 96, 60, 10)

#define MAXIMUM_NAME_LABEL_SIZE CGSizeMake (200, 20)
#define NAME_LABEL_FRAME CGRectMake(10, 91, 50, 20)
#define NAME_LABEL_OFFSET 10

#define CELL_SIZE CGSizeMake(320, 116)

@interface CustomExperienceCell()

@property (nonatomic, strong) UIImageView *nameBackImageView;
@property (nonatomic, strong) UIImageView *rateBackImageView;

@property (nonatomic, strong) UILabel *rateLabel;
@property (nonatomic, strong) StarsRateView *starsView;
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) UIImageView *backgroundImageView;

@end

@implementation CustomExperienceCell

@synthesize nameBackImageView;
@synthesize rateBackImageView;
@synthesize rateLabel;
@synthesize starsView;
@synthesize backgroundImageView;
@synthesize nameLabel;

- (void)addBackgroundView{
    
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 320, 116)];
    
    [self addSubview:self.backgroundImageView];
    
}

- (void)addNameLabel{
    
    self.nameLabel = [[UILabel alloc] initWithFrame:NAME_LABEL_FRAME];
    
    [self.nameLabel setFont:[UIFont customSearchBarFont]];
    [self.nameLabel setBackgroundColor:[UIColor clearColor]];
    [self.nameLabel setTextColor:[UIColor experienceNameColor]];
    
    [self addSubview:self.nameLabel];
    
}

- (void)addStarsImageViews{
    
    self.starsView = [[StarsRateView alloc] initWithFrame:STARS_VIEW_FRAME];
    
    [self addSubview:self.starsView];
    
}

- (void)addRateLabel{
    
    self.rateLabel = [[UILabel alloc] initWithFrame:RATE_LABEL_FRAME];
    
    self.rateLabel.textColor = [UIColor whiteColor];
    self.rateLabel.font = [UIFont customSearchBarFont];
    [self.rateLabel setBackgroundColor:[UIColor clearColor]];
    
    [self addSubview:self.rateLabel];
    
}

- (void)addPersonIcon{
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:PERSON_ICON_FRAME];
    
    [imgView setImage:[UIImage imageNamed:@"cell_person_icon.png"]];
    
    [self addSubview:imgView];
    
}

- (void)addRateBackImageView{
    
    self.rateBackImageView = [[UIImageView alloc] initWithFrame:RATE_BACK_IMG_VIEW_FRAME];
    [self.rateBackImageView setImage:[UIImage imageNamed:@"experienceCellRateBack.png"]];
    
    [self addSubview:self.rateBackImageView];
    
}

- (void)addNameBackImageView{
    
    self.nameBackImageView = [[UIImageView alloc] initWithFrame:NAME_BACK_IMG_VIEW_FRAME];
    [self.nameBackImageView setImage:[UIImage imageNamed:@"experienceCellNameBack.png"]];
    
    [self addSubview:self.nameBackImageView];
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        [self addBackgroundView];
        [self addRateBackImageView];
        [self addNameBackImageView];
        [self addNameLabel];
        [self addRateLabel];
        [self addPersonIcon];
        [self addStarsImageViews];
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateOriginForSubviews{
    
    CGFloat nameLabelWidth = self.nameLabel.frame.size.width;
    
    CGRect oldNameBackViewFrame = self.nameBackImageView.frame;
    [self.nameBackImageView setFrame:CGRectMake(oldNameBackViewFrame.origin.x, oldNameBackViewFrame.origin.y, nameLabelWidth + NAME_LABEL_OFFSET * 2, oldNameBackViewFrame.size.height)];
    
    CGRect oldRateBackViewFrame = self.rateBackImageView.frame;
    [self.rateBackImageView setFrame:CGRectMake(self.nameBackImageView.frame.size.width, oldRateBackViewFrame.origin.y, CELL_SIZE.width - self.nameBackImageView.frame.size.width, oldRateBackViewFrame.size.height)];
    
    [self.starsView setFrame:CGRectMake(self.nameBackImageView.frame.size.width + NAME_LABEL_OFFSET, self.starsView.frame.origin.y, self.starsView.frame.size.width, self.starsView.frame.size.height)];
    
    
}

- (void)setRate:(NSNumber *)rate{
    
    [self.starsView setRate:[rate intValue]];
    
}

- (void)setName:(NSString *)name{
    
    [self.nameLabel setText:name];
    
    CGFloat newWidth = [Constants getWidthForLabelWithText:name withFont:self.nameLabel.font andSize:MAXIMUM_NAME_LABEL_SIZE];
    CGRect oldFrame = self.nameLabel.frame;
    
    [self.nameLabel setFrame:CGRectMake(oldFrame.origin.x, oldFrame.origin.y, newWidth, oldFrame.size.height)];
    
    [self updateOriginForSubviews];
    
}

- (void)setBackGroundPhotoWithURL:(NSString *)url{
    
    [self.backgroundImageView setImageWithURL:[NSURL URLWithString:url]];
    
}

- (void)setDonesNumber:(NSNumber *)donesCount{
    
    [self.rateLabel setText:[NSString stringWithFormat:@"%d%@", [donesCount intValue], ([donesCount intValue] >= 1000)?@"k":@""]]; 
    
}

@end
