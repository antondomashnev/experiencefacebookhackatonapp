//
//  TDMServerHelper.h
//  TapZombie
//
//  Created by Anton Domashnev on 03.07.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum{
    kAddExperienceRequestID,
    kDownloadExperienceRequestID
}RequestID;

@class ServerRequest;

@protocol ServerRequestDelegate <NSObject>

- (void)request:(ServerRequest*)request succeedWithData:(id)data;
- (void)request:(ServerRequest*)request withError:(NSError *)error;

@end

@interface ServerRequest : NSObject<NSURLConnectionDataDelegate>

@property (nonatomic, assign) NSNumber *requestID;
@property (nonatomic, retain) id sendingItem;
@property (nonatomic, retain) NSString *requestEndPoint;

- (void)requestWithPath:(NSString *)theRequestEndPoint andParams:(NSDictionary *)params andRequestID:(NSNumber *)theRequestID delegate:(id<ServerRequestDelegate>)theDelegate;

@end

@interface TDMServerHelper : NSObject

+ (TDMServerHelper*)sharedHelper;
- (id)initWithRequestsQueue:(NSMutableArray *)theRequestsQueue;

- (void)addNewExperinceWithName:(NSString *)name;
- (void)downloadExperience;
- (void)share;

extern NSString* const addNewExperinceSucceedNotification;
extern NSString* const addNewExperinceFailedNotification;

extern NSString* const downloadExperienceSucceedNotification;
extern NSString* const downloadExperienceFailedNotification;


@end
