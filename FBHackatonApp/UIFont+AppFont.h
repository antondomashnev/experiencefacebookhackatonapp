//
//  UIFont+AppFont.h
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (AppFont)

+ (UIFont *)customSearchBarFont;

@end
