//
//  Place.m
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "Place.h"

@implementation Place

@synthesize stringDescription;
@synthesize address;
@synthesize coreLocation;
@synthesize latitude;
@synthesize longitude;
@synthesize facebookID;

@end
