//
//  Person.h
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Person : NSObject

@property (nonatomic, strong) NSMutableArray *cases;
@property (nonatomic, strong) NSString *stringDescription;
@property (nonatomic, strong) NSString *facebookID;

@end
