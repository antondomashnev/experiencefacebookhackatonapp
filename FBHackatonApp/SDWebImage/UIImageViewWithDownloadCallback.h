//
//  UIImageViewWithDownloadCallback.h
//  InFlow
//
//  Created by Ace Vice on 1/6/12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "UIImageView+WebCache.h"
typedef void (^imageDownloadedCallback)(UIImage* image);

@interface UIImageViewWithDownloadCallback : UIImageView
@property (nonatomic, copy, readwrite) imageDownloadedCallback callback;
@end
