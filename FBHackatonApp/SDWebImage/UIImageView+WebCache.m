/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import "UIImageView+WebCache.h"
#import "SDWebImageManager.h"
#import <OpenGLES/EAGL.h>
#include <OpenGLES/ES2/gl.h>
#include <OpenGLES/ES2/glext.h>

@implementation UIImageView (WebCache)

- (UIImage*) thumbnailedImage: (UIImage*) image withSize: (CGSize) size{
    CGRect frame = [self frame];
    if(frame.size.height>0 && frame.size.width>0)
        if(image.size.height!=self.frame.size.height || image.size.width!=self.frame.size.width){
            UIGraphicsBeginImageContext(size);
            [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
            UIImage* thumb = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            return thumb;
        }
    return image;
}

- (void)setImageWithURL:(NSURL *)url
{
    [self setImageWithURL:url placeholderImage:nil];
}

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder
{
    SDWebImageManager *manager = [SDWebImageManager sharedManager];

    // Remove in progress downloader from queue
    [manager cancelForDelegate:self];
    
    if(placeholder!=nil){
        self.image = placeholder;
    }
    
    if (url)
    {
        [manager downloadWithURL:url delegate:self];
    }
}

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder inRestrictedArea: (CGSize) area{
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    // Remove in progress downloader from queue
    [manager cancelForDelegate:self];
    
    self.image = [self thumbnailedImage:placeholder withSize:area];
    
    if (url)
    {
        [manager downloadWithURL:url delegate:self];
    }
    
}

- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder shouldMask:(BOOL) mask{
    if(!mask)
        [self setImageWithURL:url placeholderImage:placeholder];
    else{
        SDWebImageManager *manager = [SDWebImageManager sharedManager];
        
        // Remove in progress downloader from queue
        [manager cancelForDelegate:self];
        
        self.image = [self thumbnailedImage:placeholder withSize:self.frame.size];
        
        if (url)
        {
            [manager downloadWithURL:url delegate:self];
        }

    }
}

- (void) setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder withPostProcessor:(UIImage *(^)(UIImage *))processingBlock andpreCachingProcessor:(UIImage *(^)(UIImage *))preCaching{
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    
    // Remove in progress downloader from queue
    [manager cancelForDelegate:self];
    UIImage *processedPlaceholder = nil;
    if(preCaching)
        processedPlaceholder = preCaching(placeholder);
    if(processingBlock)
        processedPlaceholder = processingBlock(placeholder);
    self.image = processedPlaceholder?processedPlaceholder:placeholder;
    if (url)
    {
        [manager downloadWithURL:url delegate:self andPreCachingBlock:preCaching andPostProcessingBlock:processingBlock];
    }
}

- (void)cancelCurrentImageLoad
{
    [[SDWebImageManager sharedManager] cancelForDelegate:self];
}


- (UIImage*) maskImage:(UIImage *)image withMask:(UIImage *)maskImage {
    
	CGImageRef maskRef = maskImage.CGImage; 
    
	CGImageRef mask = CGImageMaskCreate(CGImageGetWidth(maskRef),
                                        CGImageGetHeight(maskRef),
                                        CGImageGetBitsPerComponent(maskRef),
                                        CGImageGetBitsPerPixel(maskRef),
                                        CGImageGetBytesPerRow(maskRef),
                                        CGImageGetDataProvider(maskRef), NULL, false);
    
	CGImageRef masked = CGImageCreateWithMask([image CGImage], mask);
	return [UIImage imageWithCGImage:masked];
    
}


- (void)webImageManager:(SDWebImageManager *)imageManager didFinishWithImage:(UIImage *)image
{

    if(image!=nil){
        [image retain];
        
        self.image = image;
        
        [image release];
    }

}

@end
