//
//  UIImageViewWithDownloadCallback.m
//  InFlow
//
//  Created by Ace Vice on 1/6/12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "UIImageViewWithDownloadCallback.h"

@implementation UIImageViewWithDownloadCallback
@synthesize callback;

- (void) webImageManager:(SDWebImageManager *)imageManager didFinishWithImage:(UIImage *)image{
    [super webImageManager:imageManager didFinishWithImage:image];
    self.callback(image);
}

- (void) dealloc{
    self.callback = nil;
    [super dealloc];
}

@end
