/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import "SDWebImageCompat.h"
#import "SDWebImageManagerDelegate.h"
#import <QuartzCore/QuartzCore.h>

@protocol UIImageViewPrecachingProcessorDelegate

- (UIImage*) preprocessImage: (UIImage*) image whereImageURL: (NSURL*) url;

@end

@interface UIImageView (WebCache) <SDWebImageManagerDelegate>

- (UIImage*) thumbnailedImage: (UIImage*) image withSize: (CGSize) size;
- (void)setImageWithURL:(NSURL *)url;
- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder;
- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder inRestrictedArea: (CGSize) area;
- (void)setImageWithURL:(NSURL *)url placeholderImage:(UIImage *)placeholder withPostProcessor: (UIImage*(^)(UIImage* image)) processingBlock andpreCachingProcessor: (UIImage*(^)(UIImage* image)) preCaching; 

- (void)cancelCurrentImageLoad;


@end
