//
//  UIColor+AppColor.m
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "UIColor+AppColor.h"

@implementation UIColor (AppColor)

+ (UIColor*)customSearchBarTextColor{
    
    return [UIColor blackColor];
    
}

+ (UIColor *)experienceNameColor{
    
    return [UIColor colorWithRed:70./255. green:172./255. blue:251./255. alpha:1.f];
    
}

@end
