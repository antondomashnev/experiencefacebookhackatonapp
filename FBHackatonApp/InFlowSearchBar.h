//
//  InFlowSearchBar.h
//  InFlow
//
//  Created by Ace Vice on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class InFlowSearchBar;

@protocol InFlowSearchBarDelegate <NSObject>
@optional

- (void)searchBar:(InFlowSearchBar *)theSearchBar textDidChange:(NSString *)searchText;
- (void) searchBarTextDidBeginEditing:(InFlowSearchBar *)theSearchBar;
- (void) searchBarSearchButtonClicked:(InFlowSearchBar *)searchBar;

@end

@interface InFlowSearchBar : UIView<UITextFieldDelegate>
@property (nonatomic, strong) NSString* placeholder;
@property (nonatomic, unsafe_unretained) NSObject<InFlowSearchBarDelegate>* delegate;
@property (nonatomic, strong) NSString* text;
@property (nonatomic, unsafe_unretained) BOOL isEnabled;
- (void) resignFirstResponder;
@end
