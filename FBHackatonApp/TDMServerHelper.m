//
//  TDMServerHelper.m
//  TapZombie
//
//  Created by Anton Domashnev on 03.07.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "TDMServerHelper.h"
#import "JSON.h"

NSString* const serverURL = @"http://exexapp.appspot.com/";
NSString* const serverResponseStatusOk = @"ok";
NSString* const serverResponseStatusFalse = @"false";
NSString* const serverResponseStatusKey = @"status";

NSString* const internetUnreachableDescription = @"Sorry internet connection appears to be down. Try again";
NSString* const unknownErrorDescription = @"Something went wrong, we are already working on it";

NSString* const addNewExperinceSucceedNotification = @"addNewExperinceSucceedNotification";
NSString* const addNewExperinceFailedNotification = @"addNewExperinceFailedNotification";

NSString* const downloadExperienceSucceedNotification = @"downloadExperienceSucceedNotification";
NSString* const downloadExperienceFailedNotification = @"downloadExperienceFailedNotification";

@interface ServerRequest()

@property (nonatomic, assign, readwrite) NSURLConnection* theConnection;
@property (nonatomic, assign) id<ServerRequestDelegate> delegate;
@property (nonatomic ,retain) NSMutableData *responseData;

@end

@implementation ServerRequest

@synthesize delegate;
@synthesize responseData;
@synthesize sendingItem;
@synthesize requestID;
@synthesize theConnection;
@synthesize requestEndPoint;

- (id)init{
    
    self = [super init];
    if(self){
        self.responseData = [NSMutableData data];
    }
    
    return self;
    
}

- (void)dealloc{
    
    self.delegate = nil;
    
}

#pragma mark NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.requestID forKey:@"requestIDKey"];
    [aCoder encodeObject:self.requestEndPoint forKey:@"requestEndPointKey"];
    [aCoder encodeObject:self.sendingItem forKey:@"sendingParamsKey"];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    if(([self init])){
        
        self.sendingItem = [aDecoder decodeObjectForKey:@"sendingParamsKey"];
        self.requestID = [aDecoder decodeObjectForKey:@"requestIDKey"];
        self.requestEndPoint = [aDecoder decodeObjectForKey:@"requestEndPointKey"];

    }
    
    return self;
}

- (NSString *)description{
    return [NSString stringWithFormat:@"requestID: %d; requestEndPoint: %@", [self.requestID intValue], self.requestEndPoint];
}

#pragma mark Publick Interface

- (void)requestWithPath:(NSString *)theRequestEndPoint andParams:(NSDictionary *)params andRequestID:(NSNumber *)theRequestID delegate:(id<ServerRequestDelegate>)theDelegate{
    
    self.requestID = theRequestID;
    self.requestEndPoint = theRequestEndPoint;
    self.delegate = theDelegate;
    self.sendingItem = params;
    
    NSMutableString *requestPath = [NSMutableString string];
    
    NSString *fbID = [[NSUserDefaults standardUserDefaults] objectForKey:@"fbID"];
    NSString *accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"fbAccessToken"];
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@", serverURL,requestEndPoint]]];
    NSMutableData *postBody = [NSMutableData data];
    
    [requestPath appendFormat:@"&fbid=%@&fbAT=%@", fbID, accessToken];
    
    for(NSString *key in [params allKeys]){
        [requestPath appendFormat:@"&%@=%@", key, [params objectForKey:key]];
    }

    [postBody appendData:[requestPath dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:postBody];
    [[NSURLCache sharedURLCache] setMemoryCapacity:0];
	[[NSURLCache sharedURLCache] setDiskCapacity:0];
    
    NSURLConnection* theConnetion=[[NSURLConnection alloc] initWithRequest:request delegate:self];
    self.theConnection = theConnetion;
    
    theConnetion = nil;
    
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{

    [self.delegate request:self withError:error];
    
    [connection cancel];
    self.responseData=nil;
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    
    [self.responseData appendData:data];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    
    if ([response respondsToSelector:@selector(statusCode)])
    {
        int statusCode = [((NSHTTPURLResponse *)response) statusCode];
        if (statusCode >= 400)
        {
            
            NSString *errorDescription = (statusCode == 500)? unknownErrorDescription: internetUnreachableDescription;
            NSError *error = [NSError errorWithDomain:@"ExexDomain" code:statusCode userInfo:[NSDictionary dictionaryWithObject:errorDescription forKey:NSLocalizedDescriptionKey]];
            
            [self.delegate request:self withError:error];
            
            [connection cancel];
            
            self.responseData=nil;
        }
    }
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection{
    
    NSString *response = [[NSString alloc] initWithBytes: [self.responseData mutableBytes] length:[self.responseData length] encoding:NSUTF8StringEncoding];
    SBJsonParser *jsonParser = [[SBJsonParser alloc]init];
    NSDictionary *jsonDict = [jsonParser objectWithString:response];
    
    if([jsonDict objectForKey: serverResponseStatusKey] && [[jsonDict objectForKey:serverResponseStatusKey] isEqualToString:serverResponseStatusOk]){
        [self.delegate request:self succeedWithData:jsonDict];
    }
    else {
        
        NSString *errorDescription = [jsonDict objectForKey:@"description"];
        NSError *error = [NSError errorWithDomain:@"ExexDomain" code:500 userInfo:[NSDictionary dictionaryWithObject:(errorDescription)?errorDescription:unknownErrorDescription forKey:NSLocalizedDescriptionKey]];
        
        [self.delegate request:self withError:error];
        
        
    }
    
    
    self.responseData=nil;
    [connection cancel];
    
}

@end

//---------------------------HELPER----------------

@interface TDMServerHelper()<ServerRequestDelegate>

@property (nonatomic, retain) NSMutableArray *requestsQueue;

- (void)restartQueue;

@end

@implementation TDMServerHelper

static TDMServerHelper* _sharedHelper = nil; 

@synthesize requestsQueue;

- (void) postNotificationNamed: (NSString*) name withParams: (NSDictionary*) params{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:name object:self userInfo:params];
    });
}

+ (TDMServerHelper*)sharedHelper{
    @synchronized([TDMServerHelper class])                            
    {
        if(!_sharedHelper){
            if(!_sharedHelper)                                    
                _sharedHelper = [[self alloc] initWithRequestsQueue:[NSMutableArray array]];
        }
        return _sharedHelper;                                 
    }
    return nil; 
}

+(id)alloc 
{
    @synchronized ([TDMServerHelper class])                         
    {
        NSAssert(_sharedHelper == nil,
                 @"Attempted to allocated a second instance of the TDMServerHelper singleton");                
        _sharedHelper = [super alloc];
        return _sharedHelper;                               
    }
    return nil;  
}

- (id)initWithRequestsQueue:(NSMutableArray *)theRequestsQueue{

    self = [super init];
    if(self){
        
        self.requestsQueue = theRequestsQueue;
        
        [self restartQueue];
        
    }
    return self;
    
}

- (void)dealloc{
    
    self.requestsQueue = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
}

#pragma mark NSCoding

- (void)encodeWithCoder:(NSCoder *)aCoder{
    [aCoder encodeObject:self.requestsQueue forKey:@"requestsQueueKey"];
}

- (id)initWithCoder:(NSCoder *)aDecoder{
    NSMutableArray *theRequestsQueue = [aDecoder decodeObjectForKey:@"requestsQueueKey"];
    
    return [self initWithRequestsQueue:theRequestsQueue];
}

#pragma mark QueueManagement

- (void)restartQueue{
    
    for(int i = 0; i < [self.requestsQueue count]; i++){
        ServerRequest *request = [self.requestsQueue objectAtIndex:i];
        [request requestWithPath:request.requestEndPoint andParams:request.sendingItem andRequestID:request.requestID delegate:self];
    }
    
}

- (void)share{
    
    ServerRequest *request = [[ServerRequest alloc] init];
    
    [request requestWithPath:@"case/add" andParams:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInt:10002], @"expID", nil] andRequestID:[NSNumber numberWithInt:kDownloadExperienceRequestID] delegate:self];
    
}

- (void)downloadExperience{
    
    ServerRequest *request = [[ServerRequest alloc] init];
    
    [request requestWithPath:@"exps" andParams:nil andRequestID:[NSNumber numberWithInt:kDownloadExperienceRequestID] delegate:self];
    
}

- (void)addNewExperinceWithName:(NSString *)name{
    
    ServerRequest *request = [[ServerRequest alloc] init];
    
    [request requestWithPath:@"experience/add" andParams:[NSDictionary dictionaryWithObjectsAndKeys:name, @"name", nil] andRequestID:[NSNumber numberWithInt:kAddExperienceRequestID] delegate:self];
    
}

#pragma mark Server Request Delegate

- (void)request:(ServerRequest *)request withError:(NSError *)error{
    
    NSLog(@"Request failed with error %@", error.localizedDescription);
    
    switch ([request.requestID intValue]) {
        case kAddExperienceRequestID:
            [self postNotificationNamed:addNewExperinceFailedNotification withParams:error.userInfo];
        case kDownloadExperienceRequestID:
            [self postNotificationNamed:downloadExperienceFailedNotification withParams:error.userInfo];
        default:
            break;
    }
    
}

- (void)request:(ServerRequest *)request succeedWithData:(id)data{
    
    NSLog(@"Request succeed with data %@;", data);
    
    [self.requestsQueue removeObject:request];
    
    switch ([request.requestID intValue]) {
        case kAddExperienceRequestID:
            [self postNotificationNamed:addNewExperinceSucceedNotification withParams:[NSDictionary dictionaryWithObject:data forKey:@"data"]];
        case kDownloadExperienceRequestID:
            [self postNotificationNamed:downloadExperienceSucceedNotification withParams:[NSDictionary dictionaryWithObject:data forKey:@"data"]];
        default: break;
    }
    
}


@end
