//
//  ViewController.m
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "ViewController.h"
#import "InFlowSearchBar.h"
#import "SearchList.h"
#import "AddNewExperienceViewController.h"
#import "CustomSegmentedControl.h"
#import "CustomExperienceCell.h"
#import "TDMServerHelper.h"
#import "MBProgressHUD.h"
#import "Experience.h"
#import "FakeExperienceViewController.h"

NSString* const searchBarPlaceholderText = @"Search bar placeholder";

#define DEFAULT_CELL_HEIGHT 44.f
#define SEARCH_BAR_FRAME CGRectMake(0, 0, 320, 44)

#define TOP_SEGMENT_SIZE CGSizeMake(160, 46)

#define CELL_SIZE CGSizeMake(320, 116)

@interface ViewController ()<InFlowSearchBarDelegate, UITableViewDataSource, UITableViewDelegate, CustomSegmentedControlDelegate>

@property (nonatomic, strong) InFlowSearchBar *searchBar;
@property (nonatomic, strong) SearchList *searchListHelper;
@property (nonatomic, strong) NSMutableArray *experiences;
@property (nonatomic, strong) CustomSegmentedControl *topBarSegmentedControl;

- (void)drawAddNewExperienceButton;

- (void)addNewExperienceButtonClicked:(id)sender;

- (void)addExperiencesSegmentedController;
- (void)addBottomSegmentedController;

@end

@implementation ViewController

@synthesize mainTableView;
@synthesize searchBar;
@synthesize searchListHelper;
@synthesize topBarSegmentedControl;
@synthesize experiences;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(responsePopularExperiences:) name:downloadExperienceSucceedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(requestPopular) name:@"userInfoDownloaded" object:nil];
    
    self.experiences = [NSMutableArray array];
	
    [self addExperiencesSegmentedController];
    //[self drawAddNewExperienceButton];
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"fbID"]){
        [self requestPopular];
    }
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.mainTableView = nil;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark CustomSegmentedControlDelegate

- (UIButton*) buttonFor:(CustomSegmentedControl*)segmentedControl atIndex:(NSUInteger)segmentIndex{
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, TOP_SEGMENT_SIZE.width, TOP_SEGMENT_SIZE.height)];
    [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"toolbar_%d_on.png", segmentIndex]] forState:UIControlStateSelected];
    [button setImage:[UIImage imageNamed:[NSString stringWithFormat:@"toolbar_%d_off.png", segmentIndex]] forState:UIControlStateNormal];
    
    return button;
    
}

#pragma mark UI

- (void)addExperiencesSegmentedController{
    
    CustomSegmentedControl *segmentedController = [[CustomSegmentedControl alloc] initWithSegmentCount:2 segmentsize:TOP_SEGMENT_SIZE dividerImage:nil tag:0 delegate:self];
    [segmentedController setFrame:SEARCH_BAR_FRAME];
    
    [self.view addSubview:segmentedController];
    
}

- (void)drawSearchBar{
    
    self.searchBar = [[InFlowSearchBar alloc] initWithFrame:SEARCH_BAR_FRAME];
    self.searchBar.placeholder = searchBarPlaceholderText;
    self.searchBar.delegate = self;
    
    [self.view addSubview:self.searchBar];    
    
}

- (void)drawAddNewExperienceButton{
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addNewExperienceButtonClicked:)];
    
}

#pragma mark InflowSearchBar Delegate

- (void)searchBarSearchButtonClicked:(InFlowSearchBar *)searchBar{
    
    [self.searchBar resignFirstResponder];
    
}

- (void)searchBar:(InFlowSearchBar *)theSearchBar textDidChange:(NSString *)searchText{
    
    if([searchText isEqualToString:@""]){
        //TODO
    }
    else{
        //TODO
    }
    
    [self.mainTableView reloadData];
    
}

- (void)searchBarTextDidBeginEditing:(InFlowSearchBar *)theSearchBar{
    
    //TODO
    
}

#pragma mark Server

- (void)requestPopular{
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Loading";
    hud.dimBackground = YES;
    
    [[TDMServerHelper sharedHelper] downloadExperience];
    
}

- (void)responsePopularExperiences:(NSNotification *)notification{
    
    NSArray *resultArray = [[[notification userInfo] objectForKey:@"data"] objectForKey:@"exps"];
    
    for(NSDictionary *dict in resultArray){
        Experience *exp = [[Experience alloc] init];
        [exp setValuesForKeysWithDictionary:dict];
        [self.experiences addObject:exp];
    }
    
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    [self.mainTableView reloadData];
    
}

#pragma mark Action

- (void)addNewExperienceButtonClicked:(id)sender{
    
    AddNewExperienceViewController *controller = [[AddNewExperienceViewController alloc] init];
    
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    [self presentModalViewController:navController animated:YES];
    
}

#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return CELL_SIZE.height;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    FakeExperienceViewController *controller = [[FakeExperienceViewController alloc] init];
    
    UINavigationController *navContr = [[UINavigationController alloc] initWithRootViewController:controller];
    
    [self presentModalViewController:navContr animated:YES];
    
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return [self.experiences count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSString *cellIdentifier = @"myIdentifier";
    
    CustomExperienceCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if(!cell){
        cell = [[CustomExperienceCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
    }
    
    Experience *exp = [self.experiences objectAtIndex:indexPath.row];
    [cell setRate:exp.rating];
    [cell setName:exp.name];
    [cell setBackGroundPhotoWithURL:exp.photoUrl];
    [cell setDonesNumber:exp.usedCount];
    
    return cell;
    
}

#pragma mark UITableViewDelegate

@end
