//
//  Constants.m
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "Constants.h"

@implementation Constants

+(CGFloat)getWidthForLabelWithText:(NSString*)text withFont:(UIFont*)font andSize:(CGSize)size{
    CGSize dateStringSize = [text sizeWithFont:font constrainedToSize:size lineBreakMode:UILineBreakModeWordWrap];
    return dateStringSize.width;
}

@end
