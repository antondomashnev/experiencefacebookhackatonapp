//
//  CustomExperienceCell.h
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomExperienceCell : UITableViewCell

- (void)setBackGroundPhotoWithURL:(NSString *)url;
- (void)setRate:(NSNumber *)rate;
- (void)setName:(NSString *)name;
- (void)setDonesNumber:(NSNumber *)donesCount;

@end
