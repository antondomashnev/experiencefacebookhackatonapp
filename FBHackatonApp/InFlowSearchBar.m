//
//  InFlowSearchBar.m
//  InFlow
//
//  Created by Ace Vice on 3/24/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "InFlowSearchBar.h"
#import "UIColor+AppColor.h"
#import "UIFont+AppFont.h"

#define TEXT_FIELD_HEIGHT 25.
#define TEXT_FIELD_WIDTH 260.
#define TEXT_FILED_RIGHT_MARGIN 47.
#define HEIGHT 52.

@interface InFlowSearchBar(){
    BOOL shouldDisplayPlaceholder;
}
@property (nonatomic, strong) UITextField* textField;
@property (nonatomic, strong) UIButton* clearButton;
@end

@implementation InFlowSearchBar
@synthesize placeholder = _placeholder, delegate = _delegate, textField = _textField, text=_text, clearButton=_clearButton;
@synthesize isEnabled = _isEnabled;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        shouldDisplayPlaceholder = YES;
        UIImageView* background =  [[UIImageView alloc] initWithFrame:CGRectMake(frame.origin.x,frame.origin.y,320., HEIGHT)];
        background.image = [UIImage imageNamed:@"list_search"];
        [self addSubview:background];
        self.textField = [[UITextField alloc] initWithFrame:CGRectMake(TEXT_FILED_RIGHT_MARGIN, 8., TEXT_FIELD_WIDTH, 33.)];
        self.textField.font = [UIFont customSearchBarFont];
        self.textField.textColor = [UIColor customSearchBarTextColor];
        self.textField.backgroundColor = [UIColor clearColor];
        self.textField.delegate = self;
        self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        self.textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
       // self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.textField.returnKeyType = UIReturnKeySearch;
        self.textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        
    
        UIImage* clearImage = [UIImage imageNamed:@"lists_search_clear_btn"];
        self.clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.clearButton setImage:clearImage forState:UIControlStateNormal];
        [self.clearButton setFrame:CGRectMake(0, 0, clearImage.size.width, clearImage.size.height)];
        [self.clearButton addTarget:self action:@selector(clearButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        self.textField.rightView = self.clearButton;
        self.textField.rightViewMode = UITextFieldViewModeNever;
        [self addSubview:self.textField];
    }
    return self;
}

- (void) setPlaceholder:(NSString *)placeholder{
    _placeholder = placeholder;
    if([self.textField.text length]==0){
        self.textField.text = placeholder;
    }
}

- (void) setIsEnabled:(BOOL)isEnabled{
    _isEnabled = isEnabled;
    [self.textField setEnabled:isEnabled];
    [self.clearButton setEnabled:isEnabled];
}

- (void) resignFirstResponder{
    [self.textField resignFirstResponder];
}

- (void) setText:(NSString *)text{
    _text = text;
    self.textField.text = text;
}

- (void) setClearButtonConditionally{
    if([_text length]>0){
        self.textField.rightViewMode = UITextFieldViewModeAlways;
    }
    else
        self.textField.rightViewMode = UITextFieldViewModeNever;
}

- (void) textFieldDidBeginEditing:(UITextField *)textField{
    if(shouldDisplayPlaceholder){
        textField.text = @"";
        _text = @"";
        shouldDisplayPlaceholder = NO;
    }
    [self setClearButtonConditionally];
    if(self.delegate && [self.delegate respondsToSelector:@selector(searchBarTextDidBeginEditing:)]){
        [self.delegate searchBarTextDidBeginEditing:self];
    }
}

- (void) textFieldDidEndEditing:(UITextField *)textField{
    if([textField.text length]==0){
        shouldDisplayPlaceholder = YES;
        _text = @"";
        self.textField.text = self.placeholder;
    }
    else shouldDisplayPlaceholder = NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)strin{
    NSMutableString* changedT = [NSMutableString stringWithString:textField.text];
    [changedT replaceCharactersInRange:range withString:strin];
    _text= changedT;
    [self setClearButtonConditionally];
    if(self.delegate && [self.delegate respondsToSelector:@selector(searchBar:textDidChange:)]){
        [self.delegate searchBar:self textDidChange:changedT];
    }
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self resignFirstResponder];
    if(self.delegate && [self.delegate respondsToSelector:@selector(searchBarSearchButtonClicked:)]){
        [self.delegate searchBarSearchButtonClicked:self];
    }
    return YES;
}

- (void) clearButtonTapped: (id) sender{
    self.text =@"";
    if(!self.textField.isFirstResponder){
        shouldDisplayPlaceholder = YES;
        self.textField.text = self.placeholder;
    }
    else
        self.textField.text = @"";
    [self setClearButtonConditionally];
    if(self.delegate && [self.delegate respondsToSelector:@selector(searchBar:textDidChange:)]){
        [self.delegate searchBar:self textDidChange:@""];
    }
}


@end
