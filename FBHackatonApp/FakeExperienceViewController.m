//
//  FakeExperienceViewController.m
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "FakeExperienceViewController.h"
#import "AddNewCaseViewController.h"

@interface FakeExperienceViewController ()

@end

@implementation FakeExperienceViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)doneButtonClicked:(id)sender{

    AddNewCaseViewController *controller = [[AddNewCaseViewController alloc] init];

    UINavigationController *contr = [[UINavigationController alloc] initWithRootViewController:controller];

    [self presentModalViewController:contr animated:YES];

}

@end
