//
//  AddNewExperienceViewController.m
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "AddNewExperienceViewController.h"
#import "TDMServerHelper.h"
#import "MBProgressHUD.h"
#import "Constants.h"
#import "AddNewCaseViewController.h"

@interface AddNewExperienceViewController ()<UITextFieldDelegate>

@property (nonatomic, strong) MBProgressHUD *hud;

@end

@implementation AddNewExperienceViewController

@synthesize experienceNameTextField;
@synthesize addNewButton;
@synthesize hud;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addExperienceDidSucceed:) name:addNewExperinceSucceedNotification object:nil];
    
    [self addCloseButton];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
   
    self.experienceNameTextField = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark Notifications

- (void)addExperienceDidSucceed:(NSNotification *)notification{
    
    self.hud.labelText = @"Successfully added!";
    self.hud.mode = MBProgressHUDModeText;
    
    [self.hud hide:YES afterDelay:COMPLETED_HUD_SHOWING_TIME];
    
}

#pragma mark UI

- (void)addCloseButton{
    
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(closeButtonClicked:)];
    
}

#pragma mark Action

- (void)closeButtonClicked:(id)sender{
    
    [self dismissModalViewControllerAnimated:YES];
    
}

- (IBAction)addNewButtonClicked:(id)sender{
    
    /*self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.labelText = @"Loading";
    self.hud.dimBackground = YES;
    
    [[TDMServerHelper sharedHelper] addNewExperinceWithName:self.experienceNameTextField.text];*/
    
    AddNewCaseViewController *controller = [[AddNewCaseViewController alloc] init];
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    
    [self presentModalViewController:navController animated:YES];
    
}

#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    [self.addNewButton setEnabled:([textField.text length] > 0)];
    
    return YES;
    
}

@end
