//
//  Case.m
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "Case.h"

@implementation Case

@synthesize comments;
@synthesize place;
@synthesize rate;
@synthesize price;
@synthesize stringDescription;
@synthesize friends;
@synthesize photos;
@synthesize date;

@end
