//
//  Constants.h
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import <Foundation/Foundation.h>

#define COMPLETED_HUD_SHOWING_TIME 1.f

@interface Constants : NSObject

+(CGFloat)getWidthForLabelWithText:(NSString*)text withFont:(UIFont*)font andSize:(CGSize)size;

@end
