//
//  AddNewCaseViewController.m
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "AddNewCaseViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import <AddressBook/AddressBook.h>
#import "TDMServerHelper.h"

@interface AddNewCaseViewController ()<CLLocationManagerDelegate, FBFriendPickerDelegate, FBPlacePickerDelegate>

@property (strong, nonatomic) FBCacheDescriptor *placeCacheDescriptor;
@property (strong, nonatomic) NSObject<FBGraphPlace> *selectedPlace;
@property (strong, nonatomic) CLLocationManager *locationManager;
@property (strong, nonatomic) NSArray *selectedFriends;
@property (strong, nonatomic) UIImagePickerController *imagePicker;
@property (strong, nonatomic) UIImage *selectedPhoto;

@end

@implementation AddNewCaseViewController

@synthesize placeCacheDescriptor;
@synthesize selectedPlace;
@synthesize locationManager;
@synthesize selectedFriends;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureLocationManager];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)dealloc{
    
    locationManager.delegate = nil;
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark Places

- (void)setPlaceCacheDescriptorForCoordinates:(CLLocationCoordinate2D)coordinates {
    self.placeCacheDescriptor =
    [FBPlacePickerViewController cacheDescriptorWithLocationCoordinate:coordinates
                                                        radiusInMeters:1000
                                                            searchText:@"museums"
                                                          resultsLimit:50
                                                      fieldsForRequest:nil];
}

#pragma mark -
#pragma mark CLLocationManagerDelegate methods

- (void)configureLocationManager{
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
    self.locationManager.distanceFilter = 50;
    [self.locationManager startUpdatingLocation];
    
}

- (void)locationManager:(CLLocationManager *)manager 
    didUpdateToLocation:(CLLocation *)newLocation 
           fromLocation:(CLLocation *)oldLocation {
    if (!oldLocation ||
        (oldLocation.coordinate.latitude != newLocation.coordinate.latitude && 
         oldLocation.coordinate.longitude != newLocation.coordinate.longitude &&
         newLocation.horizontalAccuracy <= 100.0)) {
            // Fetch data at this new location, and remember the cache descriptor.
            [self setPlaceCacheDescriptorForCoordinates:newLocation.coordinate];
            [self.placeCacheDescriptor prefetchAndCacheForSession:FBSession.activeSession];
        }
}

- (void)locationManager:(CLLocationManager *)manager 
       didFailWithError:(NSError *)error {
	NSLog(@"%@", error);
}

#pragma mark UIImagePickerControllerDelegate methods



#pragma mark Action

- (void)doneButtonClicked:(id)sender{
    
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"finalScreen.png"]];
    
    [[[[UIApplication sharedApplication] windows] objectAtIndex:0] addSubview:imgView];
    
    [[TDMServerHelper sharedHelper] share];
    
}

- (IBAction)addCostButtonClicked:(id)sender{
    [sender setEnabled:NO];
}

- (IBAction)addRateButtonClicked:(id)sender{
    [sender setEnabled:NO];
}

- (IBAction)addCommentButtonClicked:(id)sender{
    [sender setEnabled:NO];
}

- (IBAction)tookPictureButtonClicked:(id)sender {
    
    [sender setEnabled:NO];
    
}

- (IBAction)addPeopleButtonClicked:(id)sender {
    
    FBFriendPickerViewController *friendPicker = [[FBFriendPickerViewController alloc] init];
    
    // Set up the friend picker to sort and display names the same way as the
    // iOS Address Book does.
    
    // Need to call ABAddressBookCreate in order for the next two calls to do anything.
    ABAddressBookCreate();
    ABPersonSortOrdering sortOrdering = ABPersonGetSortOrdering();
    ABPersonCompositeNameFormat nameFormat = ABPersonGetCompositeNameFormat();
    
    friendPicker.sortOrdering = (sortOrdering == kABPersonSortByFirstName) ? FBFriendSortByFirstName : FBFriendSortByLastName;
    friendPicker.displayOrdering = (nameFormat == kABPersonCompositeNameFormatFirstNameFirst) ? FBFriendDisplayByFirstName : FBFriendDisplayByLastName;
    
    [friendPicker loadData];
    [friendPicker presentModallyFromViewController:self
                                          animated:YES
                                           handler:^(FBViewController *sender, BOOL donePressed) {
                                               if (donePressed) {
                                                   self.selectedFriends = friendPicker.selection;
                                               }
                                           }];
    return;

    
}

- (IBAction)addPlaceButtonClicked:(id)sender {
    
    [sender setEnabled:NO];
    
    FBPlacePickerViewController *placePicker = [[FBPlacePickerViewController alloc] init];
    placePicker.title = @"Select a restaurant";
    
    // SIMULATOR BUG:
    // See http://stackoverflow.com/questions/7003155/error-server-did-not-accept-client-registration-68
    // at times the simulator fails to fetch a location; when that happens rather than fetch a
    // a meal near 0,0 -- let's see if we can find something good in Paris
    if (self.placeCacheDescriptor == nil) {
        [self setPlaceCacheDescriptorForCoordinates:CLLocationCoordinate2DMake(48.857875, 2.294635)];
    }
    
    [placePicker configureUsingCachedDescriptor:self.placeCacheDescriptor];
    [placePicker loadData];
    [placePicker presentModallyFromViewController:self
                                         animated:YES
                                          handler:^(FBViewController *sender, BOOL donePressed) {
                                              if (donePressed) {
                                                  self.selectedPlace = placePicker.selection;
                                              }
                                          }];


}

@end
