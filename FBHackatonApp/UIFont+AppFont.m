//
//  UIFont+AppFont.m
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "UIFont+AppFont.h"

NSString* const kDefaultFont=@"TrebuchetMS";

@implementation UIFont (AppFont)

+ (UIFont *)customSearchBarFont{
    
    return [UIFont fontWithName:kDefaultFont size:12.f];
    
}


@end
