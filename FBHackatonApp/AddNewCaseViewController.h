//
//  AddNewCaseViewController.h
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewCaseViewController : UIViewController
- (IBAction)tookPictureButtonClicked:(id)sender;
- (IBAction)addPeopleButtonClicked:(id)sender;
- (IBAction)addPlaceButtonClicked:(id)sender;
- (IBAction)addCostButtonClicked:(id)sender;
- (IBAction)addRateButtonClicked:(id)sender;
- (IBAction)addCommentButtonClicked:(id)sender;
- (IBAction)doneButtonClicked:(id)sender;

@end
