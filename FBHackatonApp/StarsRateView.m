//
//  StarsRateView.m
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "StarsRateView.h"

#define STAR_SIZE CGSizeMake(9, 9)
#define STARS_OFFSET_X 12

#define START_TAG 100

@implementation StarsRateView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        float offset = 0;
        
        for(int i = 0; i < 5; i++){
            
            UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(offset, 0, STAR_SIZE.width, STAR_SIZE.height)];
            
            [imgView setImage:[UIImage imageNamed:@"starDim.png"]];
            [imgView setTag:START_TAG + i];
            [self addSubview:imgView];
            
            offset += STARS_OFFSET_X;
            
        }

        
    }
    return self;
}

- (void)setRate:(NSInteger)rate{
    
    for(int i = 0; i < rate; i++){
        if(i < rate){
            [(UIImageView *)[self viewWithTag:(START_TAG + i)] setImage:[UIImage imageNamed:@"starNotDim.png"]];
        }
        else {
            [(UIImageView *)[self viewWithTag:(START_TAG + i)] setImage:[UIImage imageNamed:@"starDim.png"]];
        }
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
