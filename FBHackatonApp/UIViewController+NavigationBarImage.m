//
//  UINavigationController+BackgroundImage.m
//  InFlow
//
//  Created by Ace Vice on 7/11/12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "UIViewController+NavigationBarImage.h"
#import <QuartzCore/QuartzCore.h>

@implementation UIViewController (NavigationBarImage)

- (void) viewDidLoad{
    //[super viewDidLoad];
    
    if([self.navigationController.navigationBar respondsToSelector:@selector(setBackgroundImage:forBarMetrics:)] ) {
        //iOS 5 new UINavigationBar custom background
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"NavigationBar.png"] forBarMetrics: UIBarMetricsDefault];
    } 
    else
        self.navigationController.navigationBar.layer.contents=(id)[UIImage imageNamed:@"NavigationBar.png"].CGImage;
}

@end
