//
//  AddNewExperienceViewController.h
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddNewExperienceViewController : UIViewController

- (IBAction)addNewButtonClicked:(id)sender;

@property (nonatomic, strong) IBOutlet UITextField *experienceNameTextField;
@property (nonatomic, strong) IBOutlet UIButton *addNewButton; 

@end
