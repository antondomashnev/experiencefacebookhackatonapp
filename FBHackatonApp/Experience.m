//
//  Experience.m
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import "Experience.h"

@implementation Experience

@synthesize cases;
@synthesize name;
@synthesize tags;
@synthesize comments;
@synthesize usedCount;
@synthesize rating;
@synthesize photoUrl;

@end
