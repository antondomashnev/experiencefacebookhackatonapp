//
//  Experience.h
//  FBHackatonApp
//
//  Created by Anton Domashnev on 01.10.12.
//  Copyright (c) 2012 Empatika. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Experience : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSMutableArray *tags;
@property (nonatomic, strong) NSMutableArray *comments;
@property (nonatomic, strong) NSMutableArray *cases;
@property (nonatomic, strong) NSNumber *usedCount;
@property (nonatomic, strong) NSNumber *rating;
@property (nonatomic, strong) NSString *photoUrl;

@end
